package com.fxbin.zipkin.config;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import zipkin2.storage.mysql.v1.MySQLStorage;

import javax.sql.DataSource;

/**
 * created with IntelliJ IDEA.
 * author: fxbin
 * date: 2018/9/11
 * time: 0:37
 * description:
 */
@Configuration
public class DataSourceConfig {



    @Primary
    @Bean
    public MySQLStorage mySQLStorage(DataSource dataSource) {
        return MySQLStorage.newBuilder().datasource(dataSource).executor(Runnable::run).build();
    }

}
