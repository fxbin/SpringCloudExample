package com.fxbin.feign;

import com.fxbin.feign.service.ApiService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FeignApplicationTests {

    @Test
    public void contextLoads() {
    }

    @Resource
    private ApiService apiService;

    @Test
    public void test(){
        try {
            System.out.println(apiService.home());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
