package com.fxbin.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;


@EnableZuulProxy
@EnableEurekaClient
@SpringBootApplication
public class ZuulApplication {

    /**
     * 当一个服务启动多个端口时，zuul 服务网关会依次请求不同端口，以达到负载均衡的目的。
     *
     */

    public static void main(String[] args) {
        SpringApplication.run(ZuulApplication.class, args);
    }
}
