package com.fxbin.feign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * created with IntelliJ IDEA.
 * author: fxbin
 * date: 2018/8/13
 * time: 2:37
 * description:
 */
@FeignClient(name = "eureka-client", fallback = ApiServiceError.class)
public interface ApiService {

    @GetMapping("/home")
    String home();

}
