package com.fxbin.feign.controller;

import com.fxbin.feign.service.ApiService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * created with IntelliJ IDEA.
 * author: fxbin
 * date: 2018/9/2
 * time: 23:25
 * description:
 */
@RestController
public class ApiController {

    @Resource
    private ApiService apiService;

    @GetMapping("/home")
    public String home(){
        return apiService.home();
    }


}
