package com.fxbin.feign.service;

import org.springframework.stereotype.Component;

/**
 * created with IntelliJ IDEA.
 * author: fxbin
 * date: 2018/9/2
 * time: 23:24
 * description:
 */
@Component
public class ApiServiceError implements ApiService {

    @Override
    public String home() {
        return "服务发生故障！";
    }
}
