package com.fxbin.eurekaclient.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * created with IntelliJ IDEA.
 * author: fxbin
 * date: 2018/8/13
 * time: 1:49
 * description:
 */
@RefreshScope
@RestController
public class HomeController {

    @Value("${server.port}")
    public String port;

    @GetMapping("/home")
    public String home(){
        return "hi, i am from port :" + port;
    }

}
